import unittest
from unittest.mock import patch

from pr0cks_extension.cli import iter_extensible_commands, Pr0cksCommand


class MockEntryPoint:
    def __init__(self, item):
        self.item = item
        self.module_name = self.name = str(item)

    # pylint:disable=unused-argument
    def load(self, *args, **kwargs):
        return self.item


def mock_iter_entry_points(array):
    # pylint:disable=unused-argument
    def mocked(*args, **kwargs):
        for item in array:
            yield MockEntryPoint(item)

    return mocked


class ExtensibleCommandsTestCase(unittest.TestCase):
    def test_collecting_bad_commands(self):
        # pylint:disable=bad-continuation
        with patch('pkg_resources.iter_entry_points', mock_iter_entry_points([
            None,
            1,
            "something",
            (1, 2, 3),
            {}
        ])):
            # pylint:disable=unnecessary-comprehension
            self.assertListEqual(
                [item for item in iter_extensible_commands()],
                []
            )

    def test_collecting_good_commands(self):
        class TestCommand(Pr0cksCommand):
            pass

        with patch('pkg_resources.iter_entry_points', mock_iter_entry_points([
            TestCommand,
        ])):
            # pylint:disable=unnecessary-comprehension
            self.assertListEqual(
                [item for item in iter_extensible_commands()],
                [TestCommand]
            )


if __name__ == '__main__':
    unittest.main()
